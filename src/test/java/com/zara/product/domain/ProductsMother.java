package com.zara.product.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class ProductsMother {

    public static Products sameProductAndDifferentPrices(final String firstStartDate, final String firstEndDate, final String secondStartDate, final String secondEndDate) {
        final Product firstProduct = new Product(1L, 3245L, 0, LocalDateTime.parse(firstStartDate),
                LocalDateTime.parse(firstEndDate), "EUR", BigDecimal.valueOf(38.20), 1L);
        final Product secondProduct = new Product(1L, 3245L, 1, LocalDateTime.parse(secondStartDate),
                LocalDateTime.parse(secondEndDate), "EUR", BigDecimal.valueOf(25.20), 1L);

        final List<Product> products = List.of(firstProduct, secondProduct);

        return new Products(products);
    }

    public static Products productsWithDifferentPriority(final int firstPriority, final int secondPriority) {
        final Product firstProduct = new Product(1L, 3245L, firstPriority, LocalDateTime.parse("2021-01-14T10:00:00"),
                LocalDateTime.parse("2021-01-14T18:00:00"), "EUR", BigDecimal.valueOf(38.20), 1L);
        final Product secondProduct = new Product(1L, 3245L, secondPriority, LocalDateTime.parse("2021-01-10T10:00:00"),
                LocalDateTime.parse("2021-01-20T00:00:00"), "EUR", BigDecimal.valueOf(25.20), 1L);

        final List<Product> products = List.of(firstProduct, secondProduct);

        return new Products(products);
    }


}
