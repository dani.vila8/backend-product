package com.zara.product.application;

import com.zara.product.application.command.FindProductCommand;
import com.zara.product.domain.ProductRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public abstract class ProductUnitTestCase {

    @Mock
    protected ProductRepository repository;

    public void shouldBeSearch(final FindProductCommand command) {
        verify(repository, atLeastOnce()).findByProductAndBrand(command);
    }
}
