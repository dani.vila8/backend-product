package com.zara.product.application.command;

import java.time.LocalDateTime;

public class FindProductCommandMother {

    public static FindProductCommand withDate(final String date) {
        return new FindProductCommand(LocalDateTime.parse(date), 25230L, 1);
    }


}
