package com.zara.product.application.find;

import com.zara.product.application.ProductUnitTestCase;
import com.zara.product.application.command.FindProductCommand;
import com.zara.product.application.command.FindProductCommandMother;
import com.zara.product.domain.Product;
import com.zara.product.domain.ProductNotFoundException;
import com.zara.product.domain.Products;
import com.zara.product.domain.ProductsMother;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

public class ProductFinderUnitTest extends ProductUnitTestCase {

    @InjectMocks
    private ProductFinder finder;

    @Test
    void find_product_and_brand() {
        final Products products = ProductsMother.sameProductAndDifferentPrices(
                "2021-01-14T10:00:00",
                "2021-01-14T18:00:00",
                "2021-01-12T10:00:00",
                "2021-05-01T00:00:00");

        final FindProductCommand command = FindProductCommandMother.withDate("2021-01-14T12:00:00");

        given(repository.findByProductAndBrand(command)).willReturn(products);

        finder.findByProductAndBrand(command);

        shouldBeSearch(command);
    }

    @Test
    void should_return_the_one_with_the_highest_priority() {
        final Products products = ProductsMother.productsWithDifferentPriority(2, 0);
        final FindProductCommand command = FindProductCommandMother.withDate("2021-01-14T12:00:00");

        given(repository.findByProductAndBrand(command)).willReturn(products);

        final Product product = finder.findByProductAndBrand(command);

        assertEquals(2, product.priority());
    }

    @Test
    void should_return_an_error_product_not_found() {
        final Products products = ProductsMother.productsWithDifferentPriority(2, 0);
        final FindProductCommand command = FindProductCommandMother.withDate("2040-01-14T12:00:00");

        given(repository.findByProductAndBrand(command)).willReturn(products);

        assertThrows(ProductNotFoundException.class, () -> finder.findByProductAndBrand(command));
    }

}
