package com.zara.product.web;

import org.junit.jupiter.api.Test;

public class ProductControllerTest extends ApplicationTest {

    @Test
    void test_1() throws Exception {
        final String response = """
                {
                   "brandId": 1,
                   "productId": 35455,
                   "priority": 0,
                   "startDate": "2020-06-14T00:00:00",
                   "endDate": "2020-12-31T23:59:59",
                   "currency": "EUR",
                   "price": 35.50,
                   "priceList": 1
                 }
                """;

        assertResponse("/product?date=2020-06-14T10:00:00&productId=35455&brandId=1", 200, response);
    }

    @Test
    void test_2() throws Exception {
        final String response = """
                {
                   "brandId": 1,
                   "productId": 35455,
                   "priority": 1,
                   "startDate": "2020-06-14T15:00:00",
                   "endDate": "2020-06-14T18:30:00",
                   "currency": "EUR",
                   "price": 25.45,
                   "priceList": 2
                 }
                """;

        assertResponse("/product?date=2020-06-14T16:00:00&productId=35455&brandId=1", 200, response);
    }

    @Test
    void test_3() throws Exception {
        final String response = """
                {
                  "brandId": 1,
                  "productId": 35455,
                  "priority": 0,
                  "startDate": "2020-06-14T00:00:00",
                  "endDate": "2020-12-31T23:59:59",
                  "currency": "EUR",
                  "price": 35.50,
                  "priceList": 1
                }
                """;

        assertResponse("/product?date=2020-06-14T21:00:00&productId=35455&brandId=1", 200, response);
    }

    @Test
    void test_4() throws Exception {
        final String response = """
                {
                   "brandId": 1,
                   "productId": 35455,
                   "priority": 1,
                   "startDate": "2020-06-15T00:00:00",
                   "endDate": "2020-06-15T11:00:00",
                   "currency": "EUR",
                   "price": 30.50,
                   "priceList": 3
                 }
                """;

        assertResponse("/product?date=2020-06-15T10:00:00&productId=35455&brandId=1", 200, response);
    }

    @Test
    void test_5() throws Exception {
        final String response = """
                {
                   "brandId": 1,
                   "productId": 35455,
                   "priority": 1,
                   "startDate": "2020-06-15T16:00:00",
                   "endDate": "2020-12-31T23:59:59",
                   "currency": "EUR",
                   "price": 38.95,
                   "priceList": 4
                 }
                """;

        assertResponse("/product?date=2020-06-16T21:00:00&productId=35455&brandId=1", 200, response);
    }

    @Test
    void should_return_not_found_exception_when_the_product_does_not_exist_on_the_date() throws Exception {
        final String response = """
                {
                   "code": "404",
                   "message": "This product doesn't have a price on the indicated date: 2040-06-16T21:00"
                 }
                """;

        assertResponse("/product?date=2040-06-16T21:00:00&productId=35455&brandId=1", 404, response);
    }
}
