CREATE TABLE PRICES
(
    BRAND_ID   INTEGER NOT NULL,
    START_DATE TIMESTAMP    NOT NULL,
    END_DATE   TIMESTAMP    NOT NULL,
    PRICE_LIST INT     NOT NULL,
    PRODUCT_ID BIGINT  NOT NULL,
    PRIORITY   INTEGER NOT NULL,
    PRICE      DECIMAL NOT NULL,
    CURR       VARCHAR NOT NULL
);