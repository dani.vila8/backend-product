package com.zara.product.application.command;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

public record FindProductCommand(
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        LocalDateTime date,
        long productId,
        int brandId
) {
}