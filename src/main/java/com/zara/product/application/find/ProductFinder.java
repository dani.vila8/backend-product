package com.zara.product.application.find;


import com.zara.product.application.command.FindProductCommand;
import com.zara.product.domain.Product;
import com.zara.product.domain.ProductRepository;
import com.zara.product.domain.Products;
import org.springframework.stereotype.Service;

@Service
public class ProductFinder {

    private final ProductRepository repository;

    public ProductFinder(ProductRepository repository) {
        this.repository = repository;
    }

    public Product findByProductAndBrand(final FindProductCommand findProductCommand) {
        final Products products = repository.findByProductAndBrand(findProductCommand);

        return products.price(findProductCommand.date());
    }
}
