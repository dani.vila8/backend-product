package com.zara.product.infrastructure;

import com.zara.product.domain.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        return new Product(
                rs.getInt("BRAND_ID"),
                rs.getLong("PRODUCT_ID"),
                rs.getInt("PRIORITY"),
                rs.getTimestamp("START_DATE").toLocalDateTime(),
                rs.getTimestamp("END_DATE").toLocalDateTime(),
                rs.getString("CURR"),
                rs.getBigDecimal("PRICE"),
                rs.getInt("PRICE_LIST")
        );
    }
}

