package com.zara.product.infrastructure;

import com.zara.product.application.command.FindProductCommand;
import com.zara.product.domain.Product;
import com.zara.product.domain.ProductRepository;
import com.zara.product.domain.Products;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class H2ProductRepository implements ProductRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public H2ProductRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Products findByProductAndBrand(final FindProductCommand findProductCommand) {
        final String sql = """
                SELECT * FROM PRICES 
                WHERE PRODUCT_ID = :productId 
                AND BRAND_ID = :brandId
                """;

        final MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("productId", findProductCommand.productId())
                .addValue("brandId", findProductCommand.brandId());

        final List<Product> productList = jdbcTemplate.query(sql, params, new ProductMapper());

        return new Products(productList);
    }

}
