package com.zara.product.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Product(
        long brandId,
        long productId,
        int priority,
        LocalDateTime startDate,
        LocalDateTime endDate,
        String currency,
        BigDecimal price,
        long priceList
) {
}
