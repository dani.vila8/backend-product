package com.zara.product.domain;

import com.zara.product.application.command.FindProductCommand;

public interface ProductRepository {

    Products findByProductAndBrand(FindProductCommand findProductCommand);


}
