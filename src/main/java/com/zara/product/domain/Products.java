package com.zara.product.domain;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public record Products(List<Product> value) {

    public Product price(final LocalDateTime date) {
        return value.stream()
                .filter(isBetweenDate(date))
                .max(Comparator.comparing(Product::priority))
                .orElseThrow(() -> new ProductNotFoundException("This product doesn't have a price on the indicated date: " + date));
    }

    public static Predicate<Product> isBetweenDate(final LocalDateTime date) {
        return product -> product.startDate().isBefore(date) && product.endDate().isAfter(date);
    }

}
