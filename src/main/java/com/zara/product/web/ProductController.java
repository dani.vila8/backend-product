package com.zara.product.web;


import com.zara.product.application.command.FindProductCommand;
import com.zara.product.application.find.ProductFinder;
import com.zara.product.domain.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    private final ProductFinder finder;

    public ProductController(final ProductFinder finder) {
        this.finder = finder;
    }

    @GetMapping("/product")
    public ResponseEntity<Product> handle(final FindProductCommand findProductCommand) {
        final Product product = finder.findByProductAndBrand(findProductCommand);


        return ResponseEntity.ok(product);
    }



}
