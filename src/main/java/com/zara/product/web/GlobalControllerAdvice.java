package com.zara.product.web;

import com.zara.product.domain.ProductNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@RestControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<Map<String, String>> handle(final ProductNotFoundException exception) {
        final Map<String, String> body = Map.of(
                "code", "404",
                "message", exception.getMessage()
        );
        return ResponseEntity.status(404).body(body);
    }

}
